<!-- Begin Block 6 -->
	<section class="block_6" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_6' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 6 -->